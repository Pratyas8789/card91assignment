const mongoose = require('mongoose')
const schema = mongoose.Schema

const customerSchema = new schema({
    status:{
        type:String,
        required:true
    },
    attempt_no:{
        type:Number,
        required:true
    },
    credited_at:{
        type:String,
        required:true
    },
    transfer_amt:{
        type:Number,
        required:true
    },
    transfer_ccy:{
        type:String,
        required:true
    },
    customer_code:{
        type:String,
        required:true
    },
    transfer_type:{
        type:String,
        required:true
    },
    bene_full_name:{
        type:String,
        required:true
    },
    credit_acct_no:{
        type:String,
        required:true
    },
    rmtr_full_name:{
        type:String,
        required:true
    },
    bene_account_no:{
        type:String,
        required:true
    },
    rmtr_account_no:{
        type:String,
        required:true
    },
    bene_account_ifsc:{
        type:String,
        required:true
    },
    rmtr_account_ifsc:{
        type:String,
        required:true
    },
    rmtr_account_type:{
        type:String,
        required:true
    },
    rmtr_to_bene_note:{
        type:String,
        required:true
    },
    transfer_timestamp:{
        type:String,
        required:true
    },
    transfer_unique_no:{
        type:String,
        required:true
    },
    rmtr_address:{
        type:String,
        required:true
    },

},{collection:"customerDetails"})

const customerInfo = mongoose.model("customerDetails",customerSchema)

module.exports = customerInfo