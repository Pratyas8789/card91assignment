const express = require('express')
const createError = require('http-errors')
const dotenv = require('dotenv').config()
const indexRouter = require('./routes/index')
// const mongoose = require('mongoose')
const app = express()

// var db = require('./dbCustomer')

const PORT = process.env.PORT || 7000
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// mongoose.connect(process.env.MONGO_URI)
//     .then(() => console.log("connected"))
//     .catch((err)=>console.log(err))

// app.post()
app.use('/ecollect', indexRouter)


app.use((req, res, next) => {
    next(createError(404))
})

app.use(function (err, req, res, next) {
    console.log("main");
    res.status(404).json({ "Error": err.message })
})

app.listen(PORT, () => console.log(`Server listen at port ${PORT}`))