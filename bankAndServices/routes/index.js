const express = require('express')
const router = express.Router()
const axios = require('axios')
const db = require('../dbCustomer')
const protect = require('../middleware/authMiddleware')

// const customerData = require('../data/customer')
const customerData = require('../model/customerSchema')


router.get("/tables",async(req,res)=>{
    db('users_details').select().then(result=>{
        res.json(result)
    })
    .catch(err=>{
        console.log(err.message);
        res.json({"err":err.message})
    })
})

router.post('/yes/validate', async (req, res) => {
    try {
        const { attempt_no,
            transfer_amt,
            transfer_ccy,
            customer_code,
            transfer_type,
            bene_full_name,
            rmtr_full_name,
            bene_account_no,
            rmtr_account_no,
            bene_account_ifsc,
            rmtr_account_type,
            rmtr_to_bene_note,
            transfer_timestamp,
            transfer_unique_no } = req.body

        const customerInfo = await customerData.find({ transfer_unique_no })
        if (customerInfo.length === 1) {
            if (customerInfo[0].attempt_no === attempt_no &&
                customerInfo[0].transfer_amt === transfer_amt &&
                customerInfo[0].transfer_ccy === transfer_ccy &&
                customerInfo[0].customer_code === customer_code &&
                customerInfo[0].transfer_type === transfer_type &&
                customerInfo[0].bene_full_name === bene_full_name &&
                customerInfo[0].rmtr_full_name === rmtr_full_name &&
                customerInfo[0].bene_account_no === bene_account_no &&
                customerInfo[0].rmtr_account_no === rmtr_account_no &&
                customerInfo[0].bene_account_ifsc === bene_account_ifsc &&
                customerInfo[0].rmtr_account_type === rmtr_account_type &&
                customerInfo[0].rmtr_to_bene_note === rmtr_to_bene_note &&
                customerInfo[0].transfer_timestamp === transfer_timestamp) {
                console.log("validation successfully");
                console.log({
                    "validateResponse": {
                        "decission": "pass"
                    }
                });
                res.status(200).json({
                    "validateResponse": {
                        "decission": "pass"
                    }
                })
            }
            else {
                console.log("validation unsuccessfully");
                console.log({
                    "validateResponse": {
                        "decission": "reject"
                    }
                });
                res.status(400).json({
                    "validateResponse": {
                        "decission": "reject"
                    }
                })
            }
        } else {
            console.log("customer is not in list");
            console.log({
                "validateResponse": {
                    "decission": "reject"
                }
            });
            res.status(400).json({
                "validateResponse": {
                    "decission": "reject"
                }
            })
        }
    } catch (error) {
        res.status(400).json({ "Error": error.message })
    }
})

router.post('/yes/notify', async (req, res) => {
    const info = req.body
    const authorization = req.headers.authorization
    const van = req.body.rmtr_account_no
    const amount = req.body.transfer_amt
    await axios.post("http://localhost:7000/ecollect/yes/validate", info)
        .then(async (response) => {
            await axios.post("http://localhost:7000/ecollect/load/webhook", { "amount": amount, "van": van }, {
                headers: {
                    Authorization: authorization,
                    'Content-Type': 'application/json'
                }
            })
                .then((response) => {
                    res.status(200).json({ "message": response.data })
                })
                .catch((error) => {
                    res.status(400).json({ "message": error.response.data })
                })
        })
        .catch((error) => {
            console.log({ "notifyResult": "not ok" })
            res.status(400).json({ "notifyResult": error.response.data })
        })
})

router.post('/load/webhook', protect, async (req, res) => {
    const { van, amount } = req.body
    if (van && amount) {
        const response = await axios.get("https://jsonplaceholder.typicode.com/todos/", { timeout: 150 }).catch((error) => console.log({ "Error": error.message }))
        if (response == undefined) {
            console.log("transaction is unsuccessfull!!!");
            res.status(400).json({ "message": "transaction is unsuccessfull!!!" })
        }
        else {
            console.log("transaction is successfull!!!");
            res.status(200).json({ "message": "transaction is successfull!!!" })
        }
    } else {
        console.log("transaction is unsuccessfull!!!");
        res.status(400).json({ message: "not ok" })
    }
})

router.post('/newUser',  async (req, res, next) => {
    try {
        const { status,
            attempt_no,
            credited_at,
            transfer_amt,
            transfer_ccy,
            customer_code,
            transfer_type,
            bene_full_name,
            credit_acct_no,
            rmtr_full_name,
            bene_account_no,
            rmtr_account_no,
            bene_account_ifsc,
            rmtr_account_ifsc,
            rmtr_account_type,
            rmtr_to_bene_note,
            transfer_timestamp,
            transfer_unique_no,
            rmtr_address } = req.body

            const allData = req.body


        // const newUser = new customerInfo({
        //     "status": status,
        //     "attempt_no": attempt_no,
        //     "credited_at": credited_at,
        //     "transfer_amt": transfer_amt,
        //     "transfer_ccy": transfer_ccy,
        //     "customer_code": customer_code,
        //     "transfer_type": transfer_type,
        //     "bene_full_name": bene_full_name,
        //     "credit_acct_no": credit_acct_no,
        //     "rmtr_full_name": rmtr_full_name,
        //     "bene_account_no": bene_account_no,
        //     "rmtr_account_no": rmtr_account_no,
        //     "bene_account_ifsc": bene_account_ifsc,
        //     "rmtr_account_ifsc": rmtr_account_ifsc,
        //     "rmtr_account_type": rmtr_account_type,
        //     "rmtr_to_bene_note": rmtr_to_bene_note,
        //     "transfer_timestamp": transfer_timestamp,
        //     "transfer_unique_no": transfer_unique_no,
        //     "rmtr_address": rmtr_address
        // })
        // newUser.save()
        // console.log(allData);

        db("users_details").insert(allData).then((result)=>{
            console.log("result");
            console.log(result);
            res.status(200).json({ "message": "The account of newUser has created successfully"})
        })
        .catch((err)=>{
            console.log("err");
            console.log(err);
            console.log(err.message);
            res.status(200).json({ "error": err.message})
        })

    } catch (error) {
        res.status(400).json({ "Error": error.message })
    }
})

module.exports = router