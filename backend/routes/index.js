const express = require('express')
const router = express.Router()
const axios = require('axios')

const getData = async() => {
    const response = await axios.get('https://jsonplaceholder.typicode.com/todos')
    return response
}

router.get('/', async(req, res) => {
    try {
        const todos = await getData()
        res.status(200).send(todos.data)
    } catch (error) {
        res.status(400).json({"message":error.message})
    }
})

router.get('/id/:id', async(req, res) => {
    try {
        const todos = await getData()
        const filtertodos = todos.data.filter((eachTodos)=>eachTodos.id==req.params.id)
        res.status(200).send(filtertodos)
    } catch (error) {
        res.status(400).json({"message":error.message})
    }
})

router.get('/userid/:id', async(req, res) => {
    try {
        const todos = await getData()
        const filtertodos = todos.data.filter((eachTodos)=>eachTodos.userId==req.params.id)
        res.status(200).send(filtertodos)
    } catch (error) {
        res.status(400).json({"message":error.message})
    }
})

router.post('/:item', (req, res) => {
    res.status(200).json({"message":`${req.params.item} posted successfully`})
})
router.delete('/:id', (req, res) => {
    res.status(200).json({"message":`${req.params.id} delete successfully`})
})
router.put('/:id', (req, res) => {
    res.status(200).json({"message":`${req.params.id} updated successfully`})
})

module.exports = router