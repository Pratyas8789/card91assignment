const expres = require('express')
const createError = require('http-errors')
const dotenv = require('dotenv')
dotenv.config()
const indexRouter = require('../routes/index')
const app = expres()
const PORT = process.env.PORT || 3007

app.use('/api', indexRouter)

app.use((req, res, next) => {
    next(createError(404))
})

app.use(function (err, req, res, next) {
    res.json({ "Error": err.message })
});


app.listen(PORT, () => console.log(`server started at PORT ${PORT}`))