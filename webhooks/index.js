const express = require('express')
const bodyParser = require('body-parser')

const path = require('path')

const PublishableKey = "pk_test_51N7J0USCcJVHCBV0Om1txUaiY4j7CUq0BoTjrOiZjtmH4fMJHuGdciQq0IvVubkKttQYpDBaOEKUUZammzEk86b3001MuHnLdH"
const Secretkey = "sk_test_51N7J0USCcJVHCBV0GXXwiw5Y5fVgZUI9X6BYVWC4dcQ4HwsQB5qI4lpqoVieu6GrfzfORXaIZtFLNJPxhOjSmJQI00ITNWL4wI"
// pk_test_51N7J0USCcJVHCBV0Om1txUaiY4j7CUq0BoTjrOiZjtmH4fMJHuGdciQq0IvVubkKttQYpDBaOEKUUZammzEk86b3001MuHnLdH
// sk_test_51N7J0USCcJVHCBV0GXXwiw5Y5fVgZUI9X6BYVWC4dcQ4HwsQB5qI4lpqoVieu6GrfzfORXaIZtFLNJPxhOjSmJQI00ITNWL4wI

const stripe = require('stripe')(Secretkey)

const app = express()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.set("view engine", "ejs")

const PORT = process.env.PORT || 3000

app.get('/', (req, res) => {
    res.render('Home', {
        key: PublishableKey
    })
})

app.post('/payment', (req, res) => {
    console.log("god");
    stripe.customers.create({
        email: req.body.stripeEmail,
        source: req.body.stripeToken,
        name: "God",
        address:{
            line1:"cloud",
            postal_code:"789",
            city:"all",
            country:"world"
        }
    })
        .then((customer) => {
            console.log("2");
            return stripe.charges.create({
                amount: 7000,
                description: "helping me",
                currency: "inr",
                customer: customer.id
            })

        })
        .then((charge) => {
            console.log("charges");
            console.log(charge);
            res.send("success")
        })
        .catch((err) => {
            console.log(err.message);
            res.send(err.message)
        })
})

app.listen(PORT, console.log("server start"))