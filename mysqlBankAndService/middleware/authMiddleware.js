const protect = async (req, res, next) => {
    let token
    const authorization = req.headers.authorization
    if (authorization) {
        token = req.headers.authorization
        if (token === "Basic ZWNvbGxlY3R5ZXNwcmQ6c2RmdWZlcjgzNDY1JiZydA==") {
            next()
        }
        else {
            res.status(400).json({ "Error": "token is not valid" })
        }
    }
    if (!token) {
        res.status(400).json({ "Error": "Not token" })
    }
}
module.exports = protect