const express = require('express')
const router = express.Router()
const axios = require('axios')
const db = require('../dbconn')
const protect = require('../middleware/authMiddleware')

router.post('/addNewCustomer', (req, res) => {
    const customerInfo = req.body
    db('users_datas').insert(customerInfo)
        .then((result) => {
            res.status(200).send(result)
        })
        .catch((err) => {
            res.status(400).send({ "Error": err.message })
        })
})

router.get('/allCustomer', (req, res) => {
    db('users_datas').select()
        .then((result) => {
            res.send(result)
        })
        .catch((err) => {
            res.send({ "Error": err.message })
        })
})

router.post('/findCustomer', (req, res) => {
    const { transfer_unique_no } = req.body
    console.log(transfer_unique_no);
    db('users_datas').where("transfer_unique_no", transfer_unique_no)
        .then((result) => {
            if (result.length > 0) {
                res.status(200).send(result)
            } else {
                res.status(400).json({ "error": "transfer_unique_no is not valid" })
            }
        })
        .catch((err) => {
            res.status(400).send({ "Error": err.message })
        })
})

router.post('/yes/validate', async (req, res) => {
    try {
        const { attempt_no,
            transfer_amt,
            transfer_ccy,
            customer_code,
            transfer_type,
            bene_full_name,
            rmtr_full_name,
            bene_account_no,
            rmtr_account_no,
            bene_account_ifsc,
            rmtr_account_type,
            rmtr_to_bene_note,
            transfer_timestamp,
            transfer_unique_no } = req.body

        await axios.post("http://localhost:7000/ecollect/findCustomer", { transfer_unique_no: transfer_unique_no })
            .then((customerInfo) => {
                if (customerInfo.data.length === 1) {
                    if (customerInfo.data[0].attempt_no === attempt_no &&
                        customerInfo.data[0].transfer_amt === transfer_amt &&
                        customerInfo.data[0].transfer_ccy === transfer_ccy &&
                        customerInfo.data[0].customer_code === customer_code &&
                        customerInfo.data[0].transfer_type === transfer_type &&
                        customerInfo.data[0].bene_full_name === bene_full_name &&
                        customerInfo.data[0].rmtr_full_name === rmtr_full_name &&
                        customerInfo.data[0].bene_account_no === bene_account_no &&
                        customerInfo.data[0].rmtr_account_no === rmtr_account_no &&
                        customerInfo.data[0].bene_account_ifsc === bene_account_ifsc &&
                        customerInfo.data[0].rmtr_account_type === rmtr_account_type &&
                        customerInfo.data[0].rmtr_to_bene_note === rmtr_to_bene_note &&
                        customerInfo.data[0].transfer_timestamp === transfer_timestamp) {
                        console.log("validation successfully");
                        console.log({
                            "validateResponse": {
                                "decission": "pass"
                            }
                        });
                        res.status(200).json({
                            "validateResponse": {
                                "decission": "pass"
                            }
                        })
                    }
                    else {
                        console.log("validation unsuccessfully");
                        console.log({
                            "validateResponse": {
                                "decission": "reject"
                            }
                        });
                        res.status(400).json({
                            "validateResponse": {
                                "decission": "reject"
                            }
                        })
                    }
                } else {
                    console.log("customer is not in list");
                    console.log({
                        "validateResponse": {
                            "decission": "reject"
                        }
                    });
                    res.status(400).json({
                        "validateResponse": {
                            "decission": "reject"
                        }
                    })
                }
            })
            .catch((err) => {
                console.log(err.message);
                res.status(400).json({
                    "validateResponse": {
                        "decission": "reject"
                    }
                })
            })
    } catch (error) {
        res.status(400).json({ "Error": error.message })
    }
})

router.post('/yes/notify', async (req, res) => {
    const info = req.body
    const authorization = req.headers.authorization
    const van = req.body.transfer_unique_no
    const amount = req.body.transfer_amt
    await axios.post("http://localhost:7000/ecollect/yes/validate", info)
        .then(async (response) => {
            await axios.post("http://localhost:7000/ecollect/load/webhook", { "amount": amount, "van": van }, {
                headers: {
                    Authorization: authorization,
                    'Content-Type': 'application/json'
                }
            })
                .then((response) => {
                    res.status(200).json({ "message": response.data })
                })
                .catch((error) => {
                    res.status(400).json({ "message": error.response.data })
                })
        })
        .catch((error) => {
            console.log({ "notifyResult": "not ok" })
            res.status(400).json({ "notifyResult": error.response.data })
        })
})

router.post('/load/webhook', protect, async (req, res) => {
    const { van, amount } = req.body
    console.log(van, amount);
    if (van && amount) {
        await axios.post("http://localhost:7000/ecollect/findCustomer", { "transfer_unique_no": van })
            .then((response) => {
                console.log(response.data);
                if (response.data.length > 0) {
                    res.status(200).json({ "message": "transaction is successfull!!" })
                } else {
                    res.status(400).json({ "message": "transaction is not successfull!!" })
                }
            })
            .catch((err) => {
                console.log(err.response.data);
                res.status(400).json({ "message": "transaction is not successfull!!" })
            })
    } else {
        console.log("transaction is unsuccessfull!!!");
        res.status(400).json({ message: "van or amount are required!!!" })
    }
})

module.exports = router