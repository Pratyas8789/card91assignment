const express = require('express')
const createError = require('http-errors')
const dotenv = require('dotenv').config()
const bodyParser = require('body-parser')
const indexRouter = require('./routes/index')
const app = express()

const PORT = process.env.PORT || 7000
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))

app.use('/ecollect',indexRouter)

app.use((req,res,next)=>{
    next(createError(404))
})

app.use(function (err,req,res,next) {
    res.status(404).json({"Error":err.message})
})

app.listen(PORT, ()=>{
    console.log(`server listen at port ${PORT}`);
})

