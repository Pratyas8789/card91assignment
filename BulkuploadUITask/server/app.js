const express = require('express')
const createError = require('http-errors')
const dotenv = require('dotenv').config()
const indexRouter = require('./route/index')
const cors = require('cors')
const app = express()
const PORT = process.env.PORT || 9000
app.use(express.json())
app.use(express.urlencoded({extended:false}))
app.use(cors())

app.use('/bulk', indexRouter)

app.use((req,res,next)=>{
    next(createError(404))
})

app.use(function (err,req,res,next) {
    res.status(404).json({"Error":err.message})
})

app.listen(PORT, ()=>{
    console.log(`Server started at PORT ${PORT}`);
})