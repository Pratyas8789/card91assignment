const express = require('express')
const multer = require('multer')
const csv = require('csv-parser')
const fs = require('fs')
const knex = require('../dbconn')
const router = express.Router()
const axios = require('axios')
const db = require('../dbconn')
const category = require('../category')

const papa = require('papaparse')

const upload = multer({ dest: 'uplods/' })

router.post('/upload/:fileName', upload.single('csvFile'), (req, res) => {
    const { fileName } = req.params
    let TotalRequest = 0
    let pending = 0

    const file = req.file
    if (!file) {
        return res.status(400).json({ "errors": 'No file uploaded' })
    }

    fs.createReadStream(file.path)
        .pipe(csv())
        .on('data', async (row) => {
            TotalRequest++
            for (const key in row) {
                if (row[key] == "") {
                    pending++
                    return;
                }
            }
        })

    fs.readFile(file.path, 'utf8', (err, data) => {
        if (err) {
            console.error(err);
            return res.status(500).send('Error reading the CSV file');
        }
        console.log(TotalRequest + " ", pending);

        const buffer = Buffer.from(data, 'utf8');

        const base64Data = buffer.toString('base64');
        
        const date = new Date()
        const info = {
            "FileName": fileName,
            "UploadedDate": (date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + Date.now()),
            "uploadedBy": "Pratyas kumar",
            "uploadedCSV": base64Data,
            "TotalRequest": TotalRequest,
            "CreatedRequest": (TotalRequest - pending),
            "FailedRequest": pending
        }
        console.log(info);
        db("bulk_schema").insert(info)
            .then((result) => {
                console.log(result);
                res.status(200).send(result)
            })
            .catch((err) => {
                console.log(err.message);
                res.status(400).send({ "Error": err.message })
            })
    });
})

router.get('/getdata', (req, res) => {
    db('bulk_schema').select()
        .then((result) => {
            console.log(result);
            res.send(result)
        })
        .catch((err) => {
            console.log(err.message);
            res.send({ "Error": err.message })
        })
})

router.get('/category', (req, res) => {
    try {
        res.status(200).send(category)
    } catch (error) {
        res.send(400).json({ "Error": error.message })
    }
})

router.delete('/deletecategory/:date', (req, res) => {
    const {date} = req.params
    try {
        db('bulk_schema')
            .where('UploadedDate', date)
            .delete()
            .then((response) => {
                res.status(200).send("deleted successfully!!!")
            })
            .catch((error) => {
                res.status(400).json({"Error":error.message})
            })
    } catch (error) {
        res.send(400).json({ "Error": error.message })
    }
})

router.post('/info', (req, res) => {    
    const { base64link } = req.body
    const decodedData = Buffer.from(base64link, 'base64').toString('utf-8');

    try {
        papa.parse(decodedData, {
            header: true,
            complete: (result) => {
                res.json(result.data);
            },
            error: (error) => {
                res.status(500).json({ error: 'Error parsing CSV file' });
            },
        });
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
});

module.exports = router