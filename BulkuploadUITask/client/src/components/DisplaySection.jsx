import React, { useEffect, useState } from 'react'
import { FaGreaterThan } from 'react-icons/fa'
import axios from 'axios'
import Example from './OffCanvas'
import { AiOutlineCloudDownload } from "react-icons/ai";
//AiOutlineCloudDownload
export default function dipalaySection(props) {
  const { display } = props
  const [show, setShow] = useState("")
  const [categoryDetails, setCategoryDetails] = useState("")
  const handeshow = () => {
    if (show) {
      setShow("")
    } else {
      setShow("show")
    }
  }

  useEffect(() => {
    axios.get("http://localhost:9000/bulk/getdata")
      .then((response) => {
        setCategoryDetails(response.data)
      })
      .catch((err) => {
        console.log(err.message);
      })
  }, [display])

  let displayData
  if (categoryDetails !== "" && Array.isArray(categoryDetails)) {
    displayData = categoryDetails.map((eachDetails) => {
      return (
        <div key={eachDetails.UploadedDate} className='d-flex w-100 h-25 mt-3'  >
          <div className='detailsName d-flex ps-3 justify-content-center align-items-center' ><h5>{eachDetails.FileName}.csv</h5></div>
          <div className='detailsName d-flex flex-column ps-3 justify-content-center' >
            {eachDetails.UploadedDate}
          </div>
          <div className='detailsName d-flex flex-column ps-3 justify-content-center' >
            {eachDetails.uploadedBy}
          </div>
          <div className='detailsName d-flex ps-3 justify-content-center align-items-center ' ><a className='text-decoration-none text-light' download={`${eachDetails.FileName}.csv`} href={`data:@file/csv;base64,${eachDetails.uploadedCSV}`}>download file <AiOutlineCloudDownload size={30}/> </a></div>
          <div className='detailsName d-flex ps-3 justify-content-center align-items-center ' ><h5>{eachDetails.TotalRequest}</h5></div>
          <div className='detailsName d-flex ps-3 justify-content-center align-items-center' ><h5>{eachDetails.CreatedRequest}</h5></div>
          <div className='detailsName d-flex ps-3 justify-content-center align-items-center' ><h5>{eachDetails.FailedRequest}</h5></div>
          <div className='detailsName d-flex ps-3 justify-content-center align-items-center' ><h5>---</h5></div>
          <div className='d-flex ps-3 justify-content-center align-items-center ' style={{ backgroundColor:"#3A496C"}} >
            <button className='border-0 pe-3' style={{backgroundColor:"#3A496C"}} onClick={() => {
              localStorage.setItem("base64link", eachDetails.uploadedCSV)
              handeshow()
            }} ><FaGreaterThan style={{color:"white"}} /></button>
          </div>
        </div>
      )
    })
  }
  return (
    <div className="dipalaySection mt-5 p-5" >
      <div className='table d-flex'>
        <div className='detailsName1 d-flex p-3 align-items-center'><h5>FileName</h5></div>
        <div className='detailsName1 d-flex flex-column p-3 justify-content-center' >
          <h5>Updated Date</h5>
          <p>Time</p>
        </div>
        <div className='detailsName1 d-flex flex-column p-3 justify-content-center' >
          <h5>Uploaded By</h5>
          <p>Mobile No.</p>
        </div>
        <div className='detailsName1 d-flex p-3 align-items-center' ><h5>Uploaded CSV</h5></div>
        <div className='detailsName1 d-flex p-3 align-items-center' ><h5>Total Requests</h5></div>
        <div className='detailsName1 d-flex p-3 align-items-center' ><h5>Create Requests</h5></div>
        <div className='detailsName1 d-flex p-3 align-items-center' ><h5>Failed Requests</h5></div>
        <div className='detailsName1 d-flex p-3 align-items-center' ><h5>Failed CSV</h5></div>
      </div>
      {displayData}
      {show && <Example handeshow={handeshow} show={show} />}
    </div>
  )
}