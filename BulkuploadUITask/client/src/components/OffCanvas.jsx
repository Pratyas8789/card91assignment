import axios from 'axios'
import React, { useEffect, useState } from 'react'
import Offcanvas from 'react-bootstrap/Offcanvas';
import {v4 as uuidv4} from 'uuid'

function OffCanvasExample({ show, handeshow, name, ...props }) {
    const [data, setData] = useState("")
    const [key, setKey] = useState("")
    useEffect(() => {
        const link = { "base64link": localStorage.base64link }
        axios.post("http://localhost:9000/bulk/info", link)
            .then((response) => {
                const data = response.data
                const key = Object.keys(data[0])
                setKey(key)
                let allFullfilledData = data.map((eachData) => {
                    let boolean = true
                    for (const key in eachData) {
                        if (eachData[key] == "") {
                            boolean = false
                            return;
                        }
                    }
                    if (boolean) {
                        return eachData
                    }
                })
                setData(allFullfilledData)
            })
            .catch((err) => {
                console.log(err.message);
            })
    }, [])

    let heading
    if (key !== "") {
        heading = key.map((eachKey) => {
            return (
                <div className='d-flex justify-content-center align-items-center' key={uuidv4()} style={{ width: `${100 / key.length}%` }} >
                    {eachKey}
                </div>
            )
        })
    }
    let result
    if (data !== "") {
        result = data.map((eachData) => {
            if (eachData !== undefined) {
                return (
                    <div key={uuidv4()} className=' d-flex w-100' style={{ height: "50px" }}>
                        <div className='tableRow d-flex justify-content-center align-items-center ms-2 mb-2' style={{ width: `${100 / key.length}%` }} >{eachData[key[0]]}</div>
                        <div className='tableRow d-flex justify-content-center align-items-center mb-2' style={{ width: `${100 / key.length}%` }} >{eachData[key[1]]}</div>
                        <div className='tableRow d-flex justify-content-center align-items-center mb-2' style={{ width: `${100 / key.length}%` }} >{eachData[key[2]]}</div>
                        <div className='tableRow d-flex justify-content-center align-items-center mb-2' style={{ width: `${100 / key.length}%` }} >{eachData[key[3]]}</div>
                        <div className='tableRow d-flex justify-content-center align-items-center mb-2' style={{ width: `${100 / key.length}%` }} >{eachData[key[4]]}</div>
                        <div className='tableRow d-flex justify-content-center align-items-center me-2  mb-2' style={{ width: `${100 / key.length}%` }} >{eachData[key[5]]}</div>
                    </div>
                )
            }
        })
    }

    return (
        <>
        {/* #0e5797 */}
            <Offcanvas show={show} onHide={handeshow} backdrop="static" {...props} style={{ width: "1000px", backgroundColor: "#0f5796", color:"white" }} >
                <Offcanvas.Header closeButton >
                    <Offcanvas.Title> <h3><b><i>Card created successfully !!</i></b></h3></Offcanvas.Title>
                </Offcanvas.Header>
                <Offcanvas.Body >
                    <div className='d-flex justify-content-center' >
                        <img srcSet={'./img/card.png'} width={"200px"} alt="" />
                    </div>
                    <div className='d-flex flex-column m-5 rounded-2' id='showResult' style={{ backgroundColor:"#122246" }} >
                        <div className='d-flex w-100' style={{ height: "50px" }} >
                            {heading}
                        </div>
                        {result}
                    </div>
                </Offcanvas.Body>
            </Offcanvas>
        </>
    );
}

export default function Example(props) {
    const { handeshow, show } = props
    return (
        <OffCanvasExample handeshow={handeshow} show={show} placement={"end"} name={"end"} />
    );
}