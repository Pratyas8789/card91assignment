import React, { useEffect, useRef, useState } from 'react'
import Button from 'react-bootstrap/Button';
import { BsDownload } from "react-icons/bs";
import { MdCloudUpload } from "react-icons/md";
import Modal from 'react-bootstrap/Modal';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';


export default function uploadSection(props) {
    const { handleDisplay } = props
    const [file, setFile] = useState(null)
    const [category, setCategory] = useState("")
    const [toogle, setToogle] = useState(false)
    const [sendFile, setSendFile] = useState(false)
    const [fileName, setFileName] = useState("")
    const [option, setOption] = useState("")
    const [url, seturl] = useState("")
    const inputRef = useRef()
    const handleDragOver = (event) => {
        event.preventDefault()
    }

    useEffect(() => {
        axios.get("http://localhost:9000/bulk/category")
            .then((response) => {
                setCategory(response.data[0].type)
                seturl(response.data[0].url)
                setOption(() => {
                    let categoryType = response.data.map((eachCatregory) => {
                        return (
                            <option key={uuidv4()} value={[eachCatregory.type, eachCatregory.url]}>{eachCatregory.type}</option>
                        )
                    })
                    return categoryType
                })
            })
            .catch((err) => {
                console.log(err.message);
            })
    }, [])

    const handleDrop = (event) => {
        event.preventDefault()
        setFile(event.dataTransfer.files[0]);
        setToogle(!toogle)
        setFileName(event.dataTransfer.files[0].name)
    }

    const handleSendFile = () => {
        setSendFile(!sendFile)
        setToogle(!toogle)
    }

    const handleToogle = () => {
        setToogle(!toogle)
    }
    if (file && sendFile) {
        const formData = new FormData()
        formData.append("csvFile", file)
        const filename = file.name.split('.')[0]
        axios.post(`http://localhost:9000/bulk/upload/${filename}`, formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
            },
        })
            .then((response) => {
                console.log("response.data");
                console.log(response.data);
                handleDisplay()

            })
            .catch((error) => {
                console.error('Error uploading file:', error);
            })

        setSendFile(!sendFile)

        // console.log("formData");
        // console.log(formData);

        // const reader = new FileReader()
        // reader.onload = (e) => {
        //     const fileContents = e.target.result;
        //     // Process the file contents or perform any required actions
        //     console.log("fileContents");
        //     console.log(fileContents);
        // };

        // reader.readAsText(file);

    }
    const handleCategory = (event) => {
        const value = event.target.value;
        const argument = value.split(',');
        setCategory(argument[0])
        seturl(argument[1]+","+argument[2])
    }
    return (
        <div className="uploadSection mt-4  d-flex"  >
            <div className='w-50 h-100 p-5'  >
                <h1>Upload</h1>
                <h6>1. Click the sample file to download sample CSV</h6>
                <h6>2. Enter respective details in the csv</h6>
                <h6>3. Upload the file</h6>
                <div className='d-flex justify-content-between mt-5 pe-5 w-100' >
                    <div className='d-flex'>
                        <h6 className='me-3' >Select Category</h6>
                        <select className='border-0 text-white p-2' name="" id="" style={{backgroundColor:"#0F5796"}} onClick={handleCategory}>
                            {option}
                        </select>
                    </div>
                            
                    <div>
                        <Button  style={{backgroundColor:"#0F5796", height:"50px"}} className='border-0'>
                            <a className='text-decoration-none ' style={{ backgroundColor:"#0F5796", color: "white" }} download={`${category}.csv`} href={url}>
                                <BsDownload /> {category} Sample File
                            </a>
                        </Button>{' '}                        
                    </div>
                </div>
            </div>
            <div className='w-50 d-flex justify-content-center align-items-center' >

                <div className='dragFileContainer w-75 h-75 d-flex flex-column justify-content-center align-items-center'
                    onDragOver={handleDragOver}
                    onDrop={handleDrop}
                >
                    <MdCloudUpload size={70}/>
                    <p>Drag your file here</p>
                    <p>OR</p>
                    <input type="file" name="" id=""
                        multiple
                        onChange={(event) => {
                            setFile(event.target.files[0]);
                            setToogle(!toogle)
                            setFileName(event.target.files[0].name)
                        }}
                        hidden
                        ref={inputRef} />
                    <Button onClick={() => inputRef.current.click()} variant="white" style={{ color: "white" }} >Browse</Button>{' '}
                    
                </div>
            </div>
            {toogle &&
                <>
                    <Modal show={toogle} >
                        <Modal.Header closeButton onClick={handleToogle} >
                            <Modal.Title>Confirm Uploaded File</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div>
                                <h4>Select Category:</h4>
                                <select name="" id="">
                                    {option}
                                </select>
                                <div className='d-flex flex-column justify-content-center align-items-center' >
                                    <img src="https://cdn.icon-icons.com/icons2/2753/PNG/512/ext_csv_filetype_icon_176252.png" height={"100px"} alt="" srcset="" />
                                    {fileName}
                                </div>
                            </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="light" onClick={handleToogle}>cancle</Button>
                            <Button variant="primary" onClick={handleSendFile}>upload</Button>
                        </Modal.Footer>
                    </Modal>
                </>
            }
        </div>
    )
}