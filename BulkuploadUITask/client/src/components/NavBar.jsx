import React from 'react'
import Navbar from 'react-bootstrap/Navbar';

export default function NavBar() {
    return (
        <Navbar className='p-0 w-100 d-flex justify-content-between' variant="dark">
            <Navbar.Brand></Navbar.Brand>
            <Navbar.Brand className='d-flex' ><img srcSet={'./img/card.png'} width={"100px"} alt="" /> <h1><i><b>Welcome to CARD91</b></i> <img srcSet={'./img/card.png'} width={"100px"} alt="" /></h1> </Navbar.Brand>
            <Navbar.Brand ></Navbar.Brand>
            {/* <img srcSet={'./img/card.png'} width={"100px"} alt="" /> */}
        </Navbar>
    )
}