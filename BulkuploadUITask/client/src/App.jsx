import { useState } from 'react'
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import UploadSection from './components/UploadSection';
import DisplaySection from './components/DisplaySection';
import NavBar from './components/NavBar';

function App() {
  const [display, setDisplay] = useState(false)
  const handleDisplay = () => {
    setDisplay(!display)
  }
  return (
    <div className='App'>
      <NavBar />
      <UploadSection handleDisplay={handleDisplay} />
      <DisplaySection display={display} />
    </div>
  )
}
export default App